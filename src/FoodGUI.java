import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class FoodGUI {
    private JPanel root;
    private JButton agedTuna200yenButton;
    private JButton salmon150yenButton;
    private JButton yellowtail160yenButton;
    public JTextArea textArea1;
    private JButton checkOutButton;

    public JLabel total;
    private JButton octopus110yenButton;
    private JButton spearSquid120yenButton;
    private JButton shrimp180yenButton;
    private JTabbedPane tabbedPane1;
    private JButton natto130yenButton;
    private JButton salmonRoeGunkan210yenButton;
    private JButton cornMayo140yenButton;
    private JButton tunaSalad170yenButton;
    private JButton shrimpMayo190yenButton;
    private JButton salad90yenButton;
    int allmoney = 0;
    int []FoodNumber = new int[12];
    String[] FoodName = {"Aged Tuna","Salmon","Yellowtail","Shrimp","Spear Squid","Octopus",
    "Natto","Salmon Roe Gunkan","Corn Mayo","Tuna Salad","Shrimp Mayo","Salad"};
    int orderNumber;
    int[] FoodMoney = {200,150,160,180,120,110,
            130,210,140,170,190,90};
    Object[] food = {1,2,3,4,5};
    void order(int orderNumber){

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + FoodName[orderNumber] + "?",
                "OrderConfirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {

            Object youlike = JOptionPane.showInputDialog(null,
                    "How many do you want to order?", "Number of orders",
                    JOptionPane.INFORMATION_MESSAGE, null, food,food[0]);
            if(youlike != null) {

                FoodNumber[orderNumber] += (int)youlike;
                    JOptionPane.showMessageDialog(null,
                        "Thank you for ordering " + FoodName[orderNumber] + "!\nIt will be served as soon as possible.",
                        "Message", JOptionPane.INFORMATION_MESSAGE);

                textArea1.setText(FoodName[orderNumber] + "×" +FoodNumber[orderNumber]+"  "
                        + FoodMoney[orderNumber]*FoodNumber[orderNumber]+"yen\n");
                String currentText = textArea1.getText();

                for (int i = 0; i < FoodName.length; i++) {
                    if (FoodNumber[i] != 0 && i != orderNumber) {
                        textArea1.setText(currentText + FoodName[i] + "×" + FoodNumber[i] + " 　" + FoodMoney[i] * FoodNumber[i] + "yen\n");
                    }
                    else;
                    currentText = textArea1.getText();
                }
                allmoney += FoodMoney[orderNumber]*(int)youlike;
                total.setText("" + allmoney + "yen");
            }
        }
        else;
    }


    public FoodGUI() {
        Arrays.fill(FoodNumber,0);
        textArea1.setEditable(false);


        agedTuna200yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Aged Tuna.jpg")
        ));
        salmon150yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Salmon.jpg")
        ));
        yellowtail160yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Yellowtail.jpg")
        ));
        shrimp180yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Shrimp.jpg")
        ));
        spearSquid120yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Spear Squid.jpg")
        ));
        octopus110yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Octopus.jpg")
        ));
        natto130yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Natto.jpg")
        ));
        salmonRoeGunkan210yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Salmon Roe Gunkan.jpg")
        ));
        cornMayo140yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Corn Mayo.jpg")
        ));
        tunaSalad170yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tuna Salad.jpg")
        ));
        shrimpMayo190yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Shrimp Mayo.jpg")
        ));
        salad90yenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Salad.jpg")
        ));


        agedTuna200yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber = 0;
                order(orderNumber);
            }
        });
        salmon150yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber = 1;
                order(orderNumber);
            }
        });
        yellowtail160yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               orderNumber=2;
                order(orderNumber);
            }
        });
        shrimp180yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=3;
                order(orderNumber);
            }
        });
        spearSquid120yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=4;
                order(orderNumber);
            }
        });
        octopus110yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=5;
                order(orderNumber);
            }
        });
        natto130yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=6;
                order(orderNumber);
            }
        });
        salmonRoeGunkan210yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=7;
                order(orderNumber);
            }
        });
        cornMayo140yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=8;
                order(orderNumber);
            }
        });
        tunaSalad170yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=9;
                order(orderNumber);
            }
        });
        shrimpMayo190yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=10;
                order(orderNumber);
            }
        });
        salad90yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderNumber=11;
                order(orderNumber);
            }
        });



        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order check out?",
                        "OrderConfirmation",JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you! The total price is "+ allmoney + " yen",
                            "Message",JOptionPane.INFORMATION_MESSAGE);
                    allmoney = 0;
                    total.setText(""+ allmoney);
                    textArea1.setText("");
                    Arrays.fill(FoodNumber,0);
                }
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

